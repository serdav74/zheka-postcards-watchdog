import json


class Config:
    def __init__(self, filename, token=""):
        self.filename = filename
        self.token = token
        self.last_update = 0
        self.chats = []
        self.jobs = dict()
    
    def save(self):
        jdict = {
            "token": self.token,
            "chats": self.chats,
            "jobs": self.jobs,
            "last_update": self.last_update
        }
        with open(self.filename, "w") as f:
            json.dump(jdict, f, indent=4)
    
    @classmethod
    def load(cls, filename):
        # Create a new Config object
        config = cls(filename)

        with open(filename, "r") as f:
            jdict = json.load(f)
        
        config.token = jdict["token"]
        config.chats = jdict["chats"]
        config.jobs = jdict["jobs"]
        config.last_update = jdict["last_update"]
        return config
            