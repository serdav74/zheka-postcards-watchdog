from config import Config
import telebot
import requests
from datetime import datetime
from json import JSONDecodeError

DEFAULT_CONFIG_FILENAME = "config.json"
HTTP = "http://"
HTTPS = "https://"
HOSTNAME = "pozdravko.ru"


class JobData:
    def __init__(self, success, info):
        self.success = success
        self.info = info


class App:
    def __init__(self):
        # пытаемся загрузить конфиг
        try:
            config = Config.load(DEFAULT_CONFIG_FILENAME)
        except (FileNotFoundError):
            token = input("Telegram Bot API token: ")
            config = Config(DEFAULT_CONFIG_FILENAME, token)
            config.save()
        
        self.config = config
        
        self.bot = telebot.TeleBot(token=config.token, parse_mode="markdown")
        self.current_jobs = dict()
    
    """
    Проверяет входящие сообщения и меняет статус оповещений (вкл/выкл)
    """
    def checkSubscriptions(self):
        offset = self.config.last_update + 1
        updates = self.bot.get_updates(offset=offset)

        for update in updates:
            if update.message.text.startswith('/notify'):
                chat_id = update.message.chat.id
                if chat_id in self.config.chats:
                    self.config.chats.remove(chat_id)
                    self.bot.reply_to(update.message, "🔕")
                else:
                    self.config.chats.append(chat_id)
                    self.bot.reply_to(update.message, "✔️")
            
            self.config.last_update = max(self.config.last_update, update.update_id)
        self.config.save()

    """
    Запрашивает URL в repeat попыток при неудаче
    """
    def get(self, url, timeout=(3, 10), repeat=3):
        r = None
        for _ in range(repeat):
            r = requests.get(url, timeout=timeout)
            if r.status_code == 200:
                return r
        return r
    
    """
    Проверяет, есть ли у скрипта доступ в интернет
    """
    def checkConnectivity(self):
        try:
            r = self.get('https://google.com/', repeat=1)
            return r.ok
        except (IOError):
            return False
    
    """
    Проверяет, доступна ли главная страница
    """
    def checkGeneral(self):
        try:
            r = self.get(HTTPS + HOSTNAME)
            return JobData(r.ok, r.status_code)
        except (IOError) as ex:
            return JobData(False, str(type(ex).__name__))
    
    """
    Проверяет, доступен ли API
    """
    def checkAPI(self):
        try:
            r = self.get(HTTPS + HOSTNAME + '/api/')
            if r.ok:
                success = r.json()["message"] == "Hello, world"
            else:
                success = False
            return JobData(success, r.status_code)
        except (IOError, KeyError, JSONDecodeError) as ex:
            return JobData(False, str(type(ex).__name__))
    
    """
    Проверяет, доступен ли pgAdmin
    """
    def checkPGAdmin(self):
        try:
            r = self.get(HTTPS + HOSTNAME + '/service/pgadmin')
            return JobData(r.ok, r.status_code)
        except (IOError) as ex:
            return JobData(False, str(type(ex).__name__))
    
    def compareJobStatus(self):
        old = self.config.jobs
        for k, v in self.current_jobs.items():
            if k in old:
                if v.success == old[k]:
                    continue
                else:
                    return False
            else:
                return False
        return True

    """
    Рассылает оповещения
    """
    def notify(self):
        def emoji(cond):
            return '👍' if cond else '❌'
        
        jobs = self.current_jobs
        text = (""
                f"**Главная**: {emoji(jobs['general'].success)} (`{jobs['general'].info}`)\n"
                f"**API**: {emoji(jobs['api'].success)} (`{jobs['api'].info}`)\n"
                f"**pgAdmin**: {emoji(jobs['pgadmin'].success)} (`{jobs['pgadmin'].info}`)")

        for chat_id in self.config.chats:
            self.bot.send_message(
                chat_id=chat_id,
                text=text
            )

    def run(self):
        print("Script started on " + datetime.now().isoformat())

        if self.checkConnectivity():
            print("Connectivity OK, checking subs...")
            self.checkSubscriptions()

            print("Starting jobs...")
            self.current_jobs['api'] = self.checkAPI()
            print("api:", self.current_jobs['api'].success)

            self.current_jobs['general'] = self.checkGeneral()
            print("general:", self.current_jobs['general'].success)

            self.current_jobs['pgadmin'] = self.checkPGAdmin()
            print("pgadmin:", self.current_jobs['pgadmin'].success)

            if not self.compareJobStatus():
                self.notify()
                self.config.jobs = dict((k, v.success) for k, v in self.current_jobs.items())
            else:
                print("No changes.")
        else:
            print("No connectivity!")

        self.config.save()


if __name__ == "__main__":
    app = App()
    app.run()
    

